package contractprogram2;


import java.io.Serializable;

public class Person implements Serializable{
    private String username;
    private String password;
    private String name;
    private String surname;
    private int phone; 

    public Person(String username, String password, String name, String surname, int phone) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }
     
   public Person(){
   }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Person{" + "username=" + username + ", password=" + password + ", name=" + name + ", surname=" + surname + ", phone=" + phone + '}';
    }
     
    
}
