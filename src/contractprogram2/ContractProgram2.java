package contractprogram2;

import java.util.Scanner;


public class ContractProgram2 {

    private static String username;
    private static String password;

    public static void login() {
        while (true) {
            System.out.println("Login");
            inputLogin();
            if (!Data.isUserPasswordCorrect(username, password)) {
                showLoginError();
                continue;
            }
            break;
        }
        System.out.println("Login successfull");
    }

    public static void inputLogin() {
        Scanner kb = new Scanner(System.in);

        System.out.print("Username: ");
        username = kb.nextLine();
        System.out.print("Password: ");
        password = kb.nextLine();
    }

    public static void main(String[] args) {
        login();
    }


    public static void showLoginError() {
        System.out.println("Username or Password is invalid");
    }

}
